# Endouble Test Project

## Installation

#### 1- NPM

Run `npm install` to install all dependencies.

#### 2- Gulp

Run `gulp` to generate Build folder.

#### 3- Accessing

After succesfully running gulp, `build` folder must be the root of the project.

## Style Guide!

For project's styleguide, access: /styleguide.html in project root.