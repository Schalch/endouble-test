let config = getEnv();

// Setting Moment's Locale
moment.locale('pt-br');

// JQuery, why not?
$(document).ready(function() {
    // Quick way for triggering default upload button
    $('#btn_upload_resume').on('click', function() {
        event.preventDefault();
        $('#f_resume').trigger('click');
    });

    $('#btn_upload_dropbox').on('click', function() {
        event.preventDefault();
        // Initializing Dropbox Chooser
        if (Dropbox) {
            Dropbox.choose({
                success: function(files) {
                    console.log('File link: ' + files[0].link);
                },
                cancel: function() {
                    console.log('User cancelled choosing file');
                },
                extensions: ['.doc', '.docx', '.pdf', '.rtf', '.txt']
            });
        } else {
            console.warn('Dropbox SDK not loaded');
        }
    });
});

// Initializing Date Picker for birth date field, if present. We could use an .map iteration
// through elements with a specific class as well, but for the moment let's just use for the one we need.
// https://flatpickr.js.org/
if (document.getElementById('f_birthdate')) {
    if (flatpickr) {
        flatpickr(document.getElementById('f_birthdate'), {
            maxDate: '2002-12-31' // a reasonable min age
        });
    }
}

// Initializing header behavior for fancy scroll
const siteHeader = document.querySelector('header.header');
window.addEventListener('scroll', event => {
    if (siteHeader) {
        if (window.pageYOffset > 80) {
            siteHeader.classList.add('scrolling');
        } else {
            siteHeader.classList.remove('scrolling');
        }
    }
});

// Search Function
function doSearch(form) {
    let value = form.q.value;
        value = value.trim();

    if (value && value.length) {
        // Now we can send searched term to API
        /* axios.post('expected-path/in/api', {
            send data from form fields
        }).then(response => {
            // Go to search results page or do something with the response
        }).catch(err => {
            console.warn(err);
        });*/
    }

}

// Form Validation
function validateFormOnSubmit(contact) {
    let errors = 0;
        /*errors += validateName(contact.f_name);
        errors += validateEmail(contact.f_email);
        errors += validatePhone(contact.phone);*/

    if (errors > 0) {
        // Error validating fields
        console.warn(errors +' errors found during validation.');
    } else {
        // Success! We can call the submit function and send data to our beloved Restful API.
        /* axios.post('expected-path/in/api', {
            send data from form fields
        }).then(response => {
            // Call modal or do something with the response
        }).catch(err => {
            console.warn(err);
        });*/

        // Mocked example of UI during the request
        contact.application_submit.classList.add('loading');
        setTimeout(function() {
            contact.application_submit.classList.add('success');
            setTimeout(() => {
            contact.application_submit.classList.remove('success');
            contact.application_submit.classList.remove('loading');
                openModal('modal-success');
            }, 500);
        }, 1500);
    }
    return false;
}

function validateName(name) {
    let error = 0;

    if (name.value.length == 0) {
        error = 1;
    } else {
        // no errors
    }
    return error;
}

function validateEmail(email) {
    let error = 0;
        email = email.value;
        email = email.trim();
    const emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
    const illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

    if (email.value == "") {
        error = 1;
    } else if (!emailFilter.test(temail)) {
        error = 1;
    } else if (email.value.match(illegalChars)) {
        error = 1;
    } else {
        // no errors
    }
    return error;
}

function validatePhone(phone) {
    let error = 0;
    let stripped = phone.value.replace(/[\(\)\.\-\ ]/g, '');

    if (phone.value == "") {
        error = 1;
    } else if (isNaN(parseInt(stripped))) {
        error = 1;
    } else if (stripped.length < 10) {
        error = 1;
    } else {
        // no errors
    }
    return error;
}
