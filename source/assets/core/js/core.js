// Core JS

// Máscaras
let v_obj, v_fun;

function mascara(o,f) {
    v_obj = o;
    v_fun = f;
    setTimeout(execmascara,1);
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value);
}

function soNumeros(v) {
    return v.replace(/\D/g,"");
}

function soTextos(v) {
    return v.replace(/\d/g,"");
}

function numPlaca(v) {
    if (v.length <= 3) {
        v = v.replace(/\d/g,"");
    } else {
        var n = v.substr(3);
        n = n.replace(/\D/g,"");
        v = v.substr(0,3) + n;
    }
    v = v.toUpperCase();
    v = v.replace(/^(\w{3})(\d{4})/g,"$1-$2");
    v = v.substr(0,8);
    return v;
}

function numcep(v) {
    v = v.replace(/\D/g,"");
    v = v.replace(/^(\d{5})(\d)/,"$1-$2");
    return v;
}

function telefone(v) {
    v = v.replace(/\D/g,"");
    v = v.replace(/^(\d{2})(\d)/g,"($1) $2");
    v = v.replace(/(\d)(\d{4})$/,"$1-$2");
    return v;
}

function data(v) {
    v = v.replace(/\D/g,"");
    v = v.replace(/^(\d\d)(\d)/g,"$1/$2");
    v = v.replace(/(\d{2})(\d)/,"$1/$2");
    return v;
}

function reais(v) {
    v = v.replace(/\D/g,"");
    v = v.replace(/(\d{1})(\d{14})$/,"$1.$2");
    v = v.replace(/(\d{1})(\d{11})$/,"$1.$2");
    v = v.replace(/(\d{1})(\d{8})$/,"$1.$2");
    v = v.replace(/(\d{1})(\d{5})$/,"$1.$2");
    v = v.replace(/(\d{0})(\d{1,2})$/,"$1,$2");
    v = v.replace(/^()()/g,"R$ $2");
    return v;
}

function float(v) {
    return v.replace(/(\D{1,10})/g,".");
}

function numcnpj(v) {
    v=v.replace(/\D/g,"");
    v=v.replace(/^(\d{2})(\d)/,"$1.$2");
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2");
    v=v.replace(/(\d{4})(\d)/,"$1-$2");
    return v;
}

function numcpf(v) {
    v=v.replace(/\D/g,"");
    v=v.replace(/(\d{3})(\d)/,"$1.$2");
    v=v.replace(/(\d{3})(\d)/,"$1.$2");
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
    return v;
}

function numcpfcnpj(v) {
    v = soNumeros(v);

    if(v.length <= 11) {
       return numcpf(v);
    }
    else{
       return numcnpj(v);
    }
}

function numrg(v) {
    v=v.replace(/\D/g,"");
    v=v.replace(/(\d{2})(\d)/,"$1.$2");
    v=v.replace(/(\d{3})(\d)/,"$1.$2");
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
    return v;
}

function number_format(number, decimals, dec_point, thousands_sep) {
      number = (number + '')
        .replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
            .toFixed(prec);
        };

      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '')
        .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
      }
      return s.join(dec);
}
// Fim Máscaras

// UI Inputs
function startUiInputs() {
    const mdInputs = document.querySelectorAll('.ui-form-input input, .ui-form-input textarea'),
          updateFilledFields = function(fields) {
            if (fields && fields.length) {
                Array.prototype.map.call(fields, (field) => {
                    if (field.value.length > 0) {
                        field.parentNode.classList.add('active');
                    }
                });
            }
          };

    for (let i=0; i<mdInputs.length; i++) {
        // Focus
        mdInputs[i].addEventListener('focus', () => {
            mdInputs[i].parentNode.classList.add('active');
        });
        // Blur
        mdInputs[i].addEventListener('blur', () => {
            if (mdInputs[i].value == '') {
                mdInputs[i].parentNode.classList.remove('active');
            }
        });
    }

    setTimeout(() => {
        updateFilledFields(mdInputs);
    }, 50);
}
// End of UI Inputs

// Auto adjusts y-scroll position for mobile on input focus
function prepareFieldsForMobileKeyboard() {

    function createMobileKeyboardSpace(input) {
        if (whatInput && 'touch' === whatInput.ask()) {
            let rect = input.getBoundingClientRect();
            let elToScroll = input;

            while (window.getComputedStyle(elToScroll).position != 'fixed') {
                elToScroll = elToScroll.parentNode;
                if (elToScroll == document.body) {
                    break;
                }
            }

            document.body.classList.add('mobile-keyboard--opened');

            setTimeout(() => {
                if (elToScroll == document.body) {
                    window.scrollTo(0, window.scrollY + ((rect.y) - 100));
                } else {
                    elToScroll.scrollTop = elToScroll.scrollTop + ((rect.y) - 60);
                }
            }, 300);
        }
    }

    function removeMobileKeyboardSpace() {
        if (whatInput && 'touch' === whatInput.ask()) {
            document.body.classList.remove('mobile-keyboard--opened');
        }
    }

    let inputsMobile = document.querySelectorAll('input[type=text], input[type=password], input[type=email], input[type=number]');

    Array.prototype.map.call(inputsMobile, (input) => {
        input.addEventListener('focus', createMobileKeyboardSpace.bind(window, input));
        input.addEventListener('blur', removeMobileKeyboardSpace);
    });

}
// End of Auto adjusts y-scroll position for mobile on input focus

// Native Trigger for Node Elements a.k.a "$(el).trigger(event)"
Node.prototype.trigger = function(eventName) {
    let event = new Event(eventName);
    return this.dispatchEvent(event);
}

let amountScrolled = 0;

// Modals
function openModal(modalSelector) {
    let modal = document.getElementById(modalSelector) || document.getElementsByClassName(modalSelector)[0];
    let scrollBarWidth = getScrollBarWidth();

    if (modal) {
        amountScrolled = window.scrollY;
        document.body.classList.add('fixed');
        document.body.style.top = -(amountScrolled)+'px';
        document.body.style.paddingRight = scrollBarWidth+'px';
        modal.classList.add('ui-modal-open');
        modal.style.display = 'flex';
        if (window.innerWidth < 640) {
            modal.style.top = amountScrolled+'px';
        }
        setTimeout(() => {
            modal.style.opacity = '1';
        }, 10);
    } else {
        console.warn('Modal '+modalSelector+' not found in DOM.');
    }
}

function closeModal(modalSelector) {
    let modal = document.getElementById(modalSelector) || document.getElementsByClassName(modalSelector)[0];
    let scrollBarWidth = getScrollBarWidth();

    if (modal) {
        setTimeout(() => {
            document.body.classList.remove('fixed');
            document.body.removeAttribute('style');
            window.scrollTo(0, amountScrolled);
        }, 150);
        modal.classList.remove('ui-modal-open');
        modal.style.opacity = '0';
        setTimeout(() => {
            modal.removeAttribute('style');
        }, 300);
    } else {
        console.warn('Modal '+modalSelector+' not found in DOM.');
    }
}

function closeAllModals() {
    const modals = document.getElementsByClassName('ui-modal-open');
    if (modals && modals.length) {
        for (let i=0; i<modals.length; i++) {
            closeModal('ui-modal-open');
        }
    }
}

function activateModalOpeners() {
    const modalOpeners = document.querySelectorAll('[open-modal]');
    Array.prototype.map.call(modalOpeners, (button) => {
        button.addEventListener('click', function(e) {
            e.preventDefault();

            let openData     = this.getAttribute('open-modal'),
                hasSelector  = (openData.charAt(0) == '#' || openData.charAt(0) == '.'),
                openedModals = document.getElementsByClassName('ui-modal-open'),
                timer        = (openedModals && openedModals.length) ? 200 : 0;

            if (hasSelector) {
                openData = openData.replace(/[#.]/g , '');
            }

            if (timer) {
                closeAllModals();
            }

            setTimeout(() => {
                openModal(openData);
            }, timer);
        });
    });
}

function activateModalClosers() {
    window.addEventListener('keydown', function(event) {
        if (event.keyCode == 27) {
            closeAllModals();
            setTimeout(function() {
                let btnCalendar = (document.getElementsByClassName('calendar')) ? document.getElementsByClassName('calendar')[0] : null;
                if (btnCalendar) {
                    btnCalendar.classList.remove('opened');
                }
                let chevrons = document.getElementsByClassName('animated-chevron');
                for (let i=0; i<chevrons.length; i++) {
                    chevrons[i].classList.remove('active');
                }
            }, 30);
        }
    });

    let modalClosers = document.querySelectorAll('.overlay, .close, .close-modal');
    Array.prototype.map.call(modalClosers, (button) => {
        button.addEventListener('click', function(e) {
            closeAllModals();
        });
    });
}
// End of Modals

function getScrollBarWidth() {
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.width = '100px';
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // força a ter scrollbars
    outer.style.overflow = 'scroll';

    // adiciona uma div interna
    var inner = document.createElement('div');
    inner.style.width = '100%';
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove as divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}

// Mobile Menu
function startMobileMenu() {
    const mobileMenuButton = document.querySelector('.mobile-menu-button'),
        mobileMenu = document.querySelector('.mobile-menu'),
        closeMenuMobile = () => {
            mobileMenuButton.classList.remove('is-active');
            mobileMenu.classList.remove('is-opened');
            document.body.classList.remove('prevent-scroll');
            window.removeEventListener('click', closeMenuMobile);
        };

    if (mobileMenuButton && mobileMenu) {
        mobileMenuButton.addEventListener('click', function() {
            mobileMenuButton.classList.toggle('is-active');
            mobileMenu.classList.toggle('is-opened');
            if (mobileMenuButton.classList.contains('is-active')) {
                document.body.classList.add('prevent-scroll');
                setTimeout(() => {
                    window.addEventListener('click', closeMenuMobile);
                }, 1);
            }
        });
    }
}

// Starts all Core Scripts (necessary in order to be available for SPA environments)
function startCoreScripts() {
    startUiInputs();
    activateModalOpeners();
    activateModalClosers();
    startMobileMenu();

    // Mobile Only Scripts
    if (window.innerWidth < 640) {
        prepareFieldsForMobileKeyboard();
    }
}

window.addEventListener('resize', event => {

    // Mobile Only Behaviors
    let openedModal = document.querySelector('.ui-modal-open');
    if (window.innerWidth < 640) {
        if (openedModal && !openedModal.style.top != '0px') {
            let scrollAmount      = document.body.style.top;
                scrollAmount      = scrollAmount.replace('-','');
            openedModal.style.top = scrollAmount;
        }
    } else {
        if (openedModal && openedModal.style && openedModal.style.top && openedModal.style.top != '0px') {
            openedModal.style.top = '0px';
        }
    }
});

startCoreScripts();
